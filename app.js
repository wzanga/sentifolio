const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const {spawn} = require('child_process')
const cors = require('cors')
require('dotenv/config')
const app = express()

//Import routes
//const postsRoute = require('./interface/routes/posts')
const analysisRoute = require('./interface/routes/analysis')
app.use(bodyParser.json())
//app.use('/posts', postsRoute)
app.use('/analysis', analysisRoute)

//Help/Guide request
app.get('/help', (req, res)=>{
  console.log("received help request")
  let response = {
        "username": "User or entity quering the API (facultative)",
        "tickers": "list of tickers for the assets considered"
    }
  res.send(response)
})

//Connect to DB
//mongoose.connect(
//  process.env.DB_CONNECTION,
//  { useNewUrlParser: true },
//  ()=>console.log('Connected to the TechWills MongoDB database')
//)

//Start listening
app.listen(process.env.PORT,  () => console.log(`server started on port: ${process.env.PORT}`))
