from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
import csv


def getNews(tickers):
    """ Fetch the latest news from associated to each ticker
        example URL : https://finviz.com/quote.ashx?t=AMZN
    """
    news = {}
    TEMPLATE_URL="https://finviz.com/quote.ashx?t={}"
    for ticker in tickers:
        url = TEMPLATE_URL.format(ticker)
        req = Request(url=url, headers={'user-agent':'sentifolio'})
        res = urlopen(req)
        html = BeautifulSoup(res, 'html.parser')
        news[ticker] = html.find(id='news-table')
    return news


def getPayload(news):
    """ Get payload = (date, time, title) from each news """
    playload = {key:[] for key in news.keys()}
    for ticker in news.keys():
        lines = news.get(ticker).findAll('tr')
        curentDate = 'None'
        for idx, line in enumerate(lines):
            title = line.a.text
            timestamp = line.td.text.split(' ')
            if len(timestamp)==1:
                time = timestamp[0]
            else:
                curentDate, time = timestamp[:2]
            playload[ticker].append({'ticker':ticker, 'date':curentDate, 'time': time[:7],  'title':title})
    return playload


def getStockTickers(names=["nasdaq", "snp500", "nyse"]):
    """ Get a list of ticker from a list (e.g Nasdq, SNP500, CAC40)"""
    decoder = {
        "NASDAQ" : {
            "csv": "nasdaq_tickers",
            "url": "https://pkgstore.datahub.io/core/nasdaq-listings/nasdaq-listed_csv/data/7665719fb51081ba0bd834fde71ce822/nasdaq-listed_csv.csv"
        },
        "SNP500" : {
            "csv": "snp500_tickers",
            "url": "https://datahub.io/core/s-and-p-500-companies/r/constituents.csv"
        },
        "NYSE" : {
            "csv": "nyse_tickers",
            "url": "None"
        },
    }
    for name in names :
        key = name.upper()
        fpath = "./datastore/{}.csv".format(decoder[key]["csv"])
        try:
            f = open(fpath)
            f.close()
        except IOError:
            req = Request(url=decoder[key]["url"], headers={'user-agent':'sentifolio'})
            res = urlopen(req)
            res = res.read().decode('utf-8')
            with open(fpath, 'w') as f:
                writer = csv.writer(f)
                for line in res:
                    writer.writerow(line.split(','))
