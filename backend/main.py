import json
import sys
import scraper
import analyzer

def decodeArguments(request):
    """ Extract input provided to the API by the user's request """
    args = json.loads(request)
    checkArguments(args)
    return args


def checkArguments(args):
    """ Check that the request if well formed and has expected keys
        args : API input data hashmap
    """
    if "type" not in args :
        raise ValueError("Invalid arguments: the input args has no key 'type'")
    elif not isinstance(args["type"], str):
        raise ValueError("Invalid arguments: the value associated to the 'type' key must be a string")

    if "tickers" not in args :
        raise ValueError("Invalid arguments: the input args has no key 'assets'")
    elif not isinstance(args["tickers"], list):
        raise ValueError("Invalid arguments: the value associated to the 'tickers' key must be in JSON format")


def analyzeAssets(args):
    result = {}
    result = {"assets":{key:{} for key in args["tickers"]}}
    try :
        news = scraper.getNews(args["tickers"])
        data = scraper.getPayload(news)
        analyzer.computeSentiment(data)
        sentiments, start, end = analyzer.getDataframe(data)
        prices = analyzer.getStockPrices(args["tickers"], start, end)
        #analyzer.getVisualization(sentiments, result)
        #EMA = analyzer.computeEMA(prices, result)
        analyzer.getPorfolios(sentiments, prices, args["tickers"], result)
        result["time"] = {"start": str(start), "end": str(end) }
        return sendOutput(result, "status")
    except:
        return sendOutput(result, "failure")


def sendOutput(result, status):
    print("="*80, "OUTPUT", "="*80)
    args["status"] = status
    msg = {"result": result}
    print(json.dumps(msg))


if __name__ == '__main__':
    if len(sys.argv)==1:
        request = {
            "username": "TechWills",
            "type" : "portfolio",
            "tickers": ["AMZN", "FB", "GOOG", "AAPL"]
        }
        args = decodeArguments(json.dumps(request))
    else:
        args = decodeArguments(sys.argv[1])
    result = analyzeAssets(args)
