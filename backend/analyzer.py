import pandas as pd
import numpy as np
import yfinance as yf
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

import io
import base64
import time
import datetime


def im_2_b64(image):
    """ convert the array representatio of an image into its base64 representation """
    buff = io.BytesIO()
    image.save(buff, format="JPEG")
    img_str = base64.b64encode(buff.getvalue())
    return img_str


def b64_2_img(data):
    """ convert a base64 representation of an image to its array representation """
    buff = io.BytesIO(base64.b64decode(data))
    return Image.open(buff)


def plot(data):
    """ display an image """
    from PIL import Image
    im = b64_2_img(data)
    im.show()


def computeSentiment(data):
    """ Compte the compound value of sentiment analysis from the news titles """
    vader = SentimentIntensityAnalyzer()
    for ticker in data.keys():
        for idx, entry  in enumerate(data[ticker]):
            title = entry['title']
            sentiment = vader.polarity_scores(title)
            data[ticker][idx]["compound"] = sentiment['compound']


def getDataframe(data):
    """ Compte the compound value of sentiment analysis from the news titles """
    keys = data.keys()
    frames = [None] * len(keys)
    for idx, ticker in enumerate(keys):
        frames[idx] = pd.DataFrame(data[ticker])
    df = pd.concat(frames)
    df['date'] = pd.to_datetime(df.date).dt.date
    meanDF = df.groupby(['ticker', 'date']).mean()
    meanDF = meanDF.unstack()
    meanDF = meanDF.xs('compound', axis='columns').transpose()
    meanDF = meanDF.replace(np.nan, 0)
    dates = list(meanDF.index)
    return meanDF, dates[0], dates[-1]


def getVisualization(df, result, display=False):
    """ Plot the sentiments of tickers and store the image into the data structure """
    #Plot the sentiments
    df.plot(kind="bar", title="Asset sentiments")
    plt.grid(linestyle='-.', linewidth=1)
    plt.tight_layout()
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%Y'))
    plt.gca().xaxis.set_major_locator(mdates.DayLocator())
    plt.gcf().autofmt_xdate()
    #Save the image in base64 encoding
    buffer = io.BytesIO()
    plt.savefig(buffer, format='png', dpi=200)
    buffer.seek(0)
    result["sentiment-graph"] = str(base64.b64encode(buffer.getvalue()))
    buffer.close()
    plt.close('all')


def getStockPrices(tickers, start, end):
    """ Fetch stock prices for tickers in a given time interval [start,end] """
    prices = {}
    for ticker in tickers :
        try:
            stock = yf.download(ticker, start=start, end=end, progress=False)
            prices[ticker] = stock
        except Exception:
            print("Could not fetch stock prices for {}".format(ticker))
    return prices


def computeEMA(prices, result):
    """ Compute Exponential Moving Average """
    EMA = {}
    plt.figure()
    for ticker in prices.keys():
        closes = list(prices[ticker]['Close'])
        N = len(closes)
        w = 2.0 / (N+1)
        ema = [None] * N
        ema[0] = sum(closes) / N
        for i in range(1, N):
            ema[i] = closes[i] * w + ema[i-1] * (1-w)
        EMA[ticker] = ema
        xaxis = list(prices[ticker].index)
        plt.plot(xaxis, EMA[ticker], linestyle='--', label=ticker)
        plt.scatter(xaxis, closes)
    plt.legend()
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%Y'))
    plt.gca().xaxis.set_major_locator(mdates.DayLocator())
    plt.gcf().autofmt_xdate()
    plt.title("Exponential Moving Average (EMA) on closing values")
    plt.grid(linestyle='-.', linewidth=1)
    plt.tight_layout()
    buffer = io.BytesIO()
    plt.savefig(buffer, format='png', dpi=200)
    buffer.seek(0)
    result["EMA-graph"] = str(base64.b64encode(buffer.getvalue()))
    buffer.close()
    plt.close('all')
    return EMA


def extractClosingValues(tickers, prices):
    """ Extract closing values from the Yahoo Finance data """
    array = []
    for ticker in tickers :
        series = list(prices[ticker]["Close"])
        series = np.asarray(series)
        array.append(series / series[0])
    return np.asarray(array)


def getSeriesStats(series):
    """ get the covariance matrix and the mean of several time series """
    cov = np.cov(series)
    mean = [ np.mean(series[i]) for i in range(len(series)) ]
    return mean, cov


def computePortfolioStats(w, mean, cov):
    """ compute the average return and volatility of a portfolio """
    mean = np.dot(mean, w)
    variance = np.dot(w, np.dot(cov, w))
    return mean, np.sqrt(variance)


def computeSentifolioAllocation(sentiments, mean, cov, tickers, result):
    """ compute the sentiment based portfolio allocation weights, average return and volatility """
    scoreTotal = 0
    for idx, ticker in enumerate(tickers) :
        result["assets"][ticker]["mean"] = mean[idx]
        result["assets"][ticker]["std"] = np.sqrt(cov[idx][idx])
        sentiment = sentiments[ticker]
        Npositive = len(sentiment.loc[sentiment>0])
        Ntotal = len(sentiment)
        score = Npositive / Ntotal
        result["assets"][ticker]["score"] = score
        scoreTotal += score

    weights = {};  w=[0]*len(mean);
    for i, ticker in enumerate(tickers) :
        w[i] = result["assets"][ticker]["score"] / scoreTotal
        weights[ticker] = w[i]
    mean, std = computePortfolioStats(w, mean, cov)
    result["sentiFolio"] = {
        "description":"Newspapers sentiments based portfolio",
        "weights":weights,
        "mean":mean,
        "std":std
    }


def computeEquiFolioAllocation(mean, cov, tickers, result):
    """ Compute the a portfolio with equal weights for each asset """
    n = len(mean)
    weights = {};  w=[0]*n;
    for i, ticker in enumerate(tickers) :
        w[i] = 1./n
        weights[ticker] = w[i]
    mean, std = computePortfolioStats(w, mean, cov)
    result["equiFolio"] = {
        "description":"Portfolio with equal weights for each asset",
        "weights":weights,
        "mean":mean,
        "std":std
    }


def computeMarketPortfolioAllocation(mean, cov, tickers, prices, result):
    """ Compute the market portfolio """
    volums = []
    volumsTotal =0
    for ticker in tickers :
        series = list(prices[ticker]["Volume"])
        volum = np.mean(series)
        volumsTotal += volum
        volums.append(volum)

    weights = {};  w=[0]*len(mean);
    for i, ticker in enumerate(tickers) :
        w[i] = volums[i] / volumsTotal
        weights[ticker] = w[i]
    mean, std = computePortfolioStats(w, mean, cov)
    result["marketFolio"] = {
        "description":"Market portfolio",
        "weights":weights,
        "mean":mean,
        "std":std
    }


def computeMinVarianceAllocation(mean, cov, tickers, result):
    """ Compute the minimum variance portfolio with Markowitz model """
    weights = [0]*len(mean)
    result["minVarFolio"] = {
        "description":"Minimum variance portfolio",
        "weights":{},
        "mean":0,
        "std":0
    }


def getPorfolios(sentiments, prices, tickers, result):
    """ Compute different portfolio allocations :
        - sentiment based
        - minimum variance
    """
    print("="*65, "Computing portfiolio weights", "="*65)
    series = extractClosingValues(tickers, prices)
    mean, cov = getSeriesStats(series)
    computeSentifolioAllocation(sentiments, mean, cov, tickers, result)
    computeEquiFolioAllocation(mean, cov, tickers, result)
    computeMarketPortfolioAllocation(mean, cov, tickers, prices, result)
    #computeMinVarianceAllocation(mean, cov, tickers, result)
