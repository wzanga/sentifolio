# Financial Portfolio based upon news sentiment analysis

# Composition
1. API server made with Node.js and express
2. Database MongoDB
3. Backend python

https://www.diva-portal.org/smash/get/diva2:1113099/FULLTEXT01.pdf


# Installing git on the EC2 instance
Create an EC2 instance with Amazon Linux 2 with internet access
Connect to your instance using putty

Perform a quick update on your instance: sudo yum update -y
Install git in your EC2 instance : sudo yum install git -y
Check git version : git version

# Installing node
https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html
https://ourcodeworld.com/articles/read/977/how-to-deploy-a-node-js-application-on-aws-ec2-server

tmux new -s StreamSession
streamlit run sentifolio.py --server.port 3080
dettach : Ctrl+B and then D (Don’t press Ctrl when pressing D
reattach : tmux attach -t StreamSession

# cloning
create a deploy token
git clone https://<username>:<key>gitlab.com/wzanga/sentifolio.git
