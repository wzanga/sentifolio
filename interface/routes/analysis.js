const express = require('express')
const {spawn} = require('child_process')
const Mongo = require('../modules/mongo')
const Postprocess = require('../modules/postprocess')
const router = express.Router()

// Main handler of analyis requests
router.get('/', async (req, res)=>{
  console.log('Analysis Request Received')
  req.body.userIp = req.connection.remoteAddress
  const pythonProcess = spawn('python3', ['./backend/main.py', JSON.stringify(req.body)])
  pythonProcess.stdout.on('data', (data)=>{
    console.log(data.toString())
    const result = Postprocess.filterOutput(data.toString())
    res.status(200).json(result)
  })
  pythonProcess.stderr.on('data', (data)=>{
    console.log(data.toString())
  })
})

module.exports = router
