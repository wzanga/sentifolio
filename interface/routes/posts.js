const express = require('express')
const Mongo = require('../modules/mongo')
const router = express.Router()

// GET BACK ALL THE POSTS
router.get('/', async (req,res)=>{
  const posts = await Post.find()
  res.json(posts)
})

//GET SPECIFIC POSTS
router.get('/:postId', async (req,res)=>{
  const post = await Mongo.get(req.params.postId)
  res.json(post)
})

//SUBMIT A POST
router.post('/', async (req,res)=>{
  const savedPost = await Mongo.submit(req.body.title, req.body.description)
  res.json(savedPost)
})

//UPDATE SPECIFIC POSTS
router.patch('/:postId', async (req,res)=>{
  const updatedPost = await Mongo.update(req.params.postId, req.body.title)
  res.json(updatedPost)
})

//DELETE SPECIFIC POSTS
router.delete('/:postId', async (req,res)=>{
  const removePost = await Mongo.remove(req.params.postId)
  res.json(removePost)
})

module.exports = router
