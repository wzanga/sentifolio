/*
* Description : This module serves to preprocess the output of the python script
* Author : WKZ
* Year : 2021
*/

//Check if a string is a valid JSON file and
function IsJsonString(str) {
    try {
        let data = JSON.parse(str)
        return {isValid:true, payload:data}
    } catch (e) {
        return {isValid:false, payload:null}
    }
}

//Check if a stirng contains the python output
function containsResults(str){
  let jsonData = IsJsonString(str)
  let result = null
  if(jsonData.isValid){
    Object.entries(jsonData.payload).forEach(([key, value]) => {
      if(key == "result" && result==null){
        result = value
      }
    })
  }
  return {isValid:result!=null, payload:result}
}

//Return the python output of an analysis
function filterOutput(string){
  let result = null
  let messages = string.split("\n");
  messages.forEach( function (message, index) {
    if ( (message.length>0) && (message.charAt(0)=="{") && (message.slice(-1)=="}") ){
      let response = containsResults(message)
      if (response.isValid && result==null){
        result = response.payload
      }
    }
  })
  return result
}

module.exports = {filterOutput}
