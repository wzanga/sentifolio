/*
* Description : This module serves to make queries to the MongoDB databse
* Author : WKZ
* Year : 2021
*/
const Post = require('../models/Post')

//Get a specific post
async function get(postID){
  try {
    const post = await Post.findById(postID)
    return post
  }catch(err){
    return {message :err}
  }
}

//Get all the posts from the database
async function getAll(){
  try {
    const posts = await Post.find()
    return posts
  }catch(err){
    return {message :err}
  }
}

//Submit a post to the database
async function submit(postTitle, postDescription){
  const post = new Post({
    title : postTitle,
    description : postDescription
  })
  try {
    const savedPost = await post.save()
    return savedPost
  }catch(err){
    return {message :err}
  }
}

//Update a post alredy in the database
async function update(postID, title){
  try {
    const updatedPost = await Post.updateOne(
      {_id:postID},
      {$set: {title : title} }
    )
    return updatedPost
  }catch(err){
    return {message :err}
  }
}

//delete a post in the database
async function remove(postID){
  try {
    const removePost = await Post.remove({_id: postID })
    return removePost
  }catch(err){
    return {message :err}
  }
}


module.exports = {get, getAll, submit, update, remove}
