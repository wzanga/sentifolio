import streamlit as st
import pandas as pd
import base64
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import yfinance as yf
import requests
from requests.exceptions import Timeout
import json

#===============================================================================
#===============================================================================
# Web scraping of S&P 500 data
@st.cache
def load_data():
    url = 'https://en.wikipedia.org/wiki/List_of_S%26P_500_companies'
    html = pd.read_html(url, header = 0)
    df = html[0]
    return df

df = load_data()
sector = df.groupby('GICS Sector')

# Download S&P500 data
# https://discuss.streamlit.io/t/how-to-download-file-in-streamlit/1806
def filedownload(df):
    csv = df.to_csv(index=False)
    b64 = base64.b64encode(csv.encode()).decode()  # strings <-> bytes conversions
    href = f'<a href="data:file/csv;base64,{b64}" download="SP500.csv">Download CSV File</a>'
    return href

# Plot Closing Price of Query Symbol
def prices_plot(data, symbols):
    fig, axis = plt.subplots(1, 1, sharex=True)
    x = list(data.index)
    ShowPlot = False
    for i, symbol in enumerate(symbols):
        if symbol in data.index :
            axis.plot(x, list(data[symbol].Close), label=symbol)
            ShowPlot = True
    if ShowPlot :
        plt.xticks(rotation=45)
        #plt.xlabel('Date', fontweight='bold')
        plt.ylabel('Closing Price', fontweight='bold')
        plt.legend()
        plt.grid(linestyle='-.', linewidth=1)
        st.header('Stock Closing Price')
        return st.pyplot(fig)

#===============================================================================
#===============================================================================
st.title('SentiFolio Web App')

st.markdown("""
Made by : TechWills \n
This app retrieves compute a portfolio allocation of a set of companies based upon
a **sentiment analysis** which is infered by by webscraping recent financial and
business newspaper articles from FinViz
* **Data source:** [Finviz](https://finviz.com/).
* **Strategy:** [Allocation model](https://www.diva-portal.org/smash/get/diva2:1113099/FULLTEXT01.pdf).
* **Python libraries:** base64, pandas, streamlit, numpy, matplotlib, seaborn, yfinance, requests, nltk.
""")

st.header('S&P 500 Companies selection')
# selector
sorted_sector_unique = sorted( df['GICS Sector'].unique() )
selected_sector = st.multiselect('Sectors', sorted_sector_unique, sorted_sector_unique)
df_selected_sector = df[ (df['GICS Sector'].isin(selected_sector)) ]

#display table
st.write('Data Dimension: ' + str(df_selected_sector.shape[0]) + ' rows and ' + str(df_selected_sector.shape[1]) + ' columns.')
st.dataframe(df_selected_sector)
st.markdown(filedownload(df_selected_sector), unsafe_allow_html=True)

#lets try a both a text input and area as well as a date
selected_symbols = st.multiselect('Select companies:', df_selected_sector.Symbol)

if len(selected_symbols) > 0 :
    query = {
        "username"  : "SentiFolio Web App",
        "type"      : "portfolio",
        "tickers"   : [ symbol for symbol in selected_symbols],
    }
    st.write('### Sentifolio API json query', query)

    if st.button('Compute Portfolio Allocations'):
        # https://pypi.org/project/yfinance/
        data = yf.download(
                tickers = list(selected_symbols),
                period = "ytd",
                interval = "1d",
                group_by = 'ticker',
                auto_adjust = True,
                prepost = True,
                threads = True,
                proxy = None)

        try:
            URL="http://3.143.46.208:3000/analysis/"
            #URL="http://localhost:3000/analysis/"
            res = requests.request(method='get', url=URL, json=query)
            res = json.loads(res.text)
            st.write('### Server response', res)
            prices_plot(data, selected_symbols)
        except Timeout:
            print('The request timed out')
        else:
            print('The request did not time out')
